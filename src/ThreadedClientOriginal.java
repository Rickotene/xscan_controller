import jdk.jfr.Unsigned;
import org.json.JSONObject;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.lang.reflect.Field;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.nio.charset.Charset;
import java.util.LinkedHashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.beans.*;

//from   w w  w  . j av a 2 s  .  c om
public class ThreadedClientOriginal {
    public static void main(String[] args) throws Exception {

        jsonHandler jsoner = new jsonHandler();

//        final JSONObject[] currentConfig = new JSONObject[1];

        AsynchronousSocketChannel channel = AsynchronousSocketChannel.open();
        SocketAddress serverAddr = new InetSocketAddress("10.0.0.2", 2000);
        Future<Void> result = channel.connect(serverAddr);

        result.get(60, TimeUnit.SECONDS);
        System.out.println("Connected");
        Attachment attach = new Attachment();
        attach.channel = channel;
        attach.buffer = ByteBuffer.allocate(4096*4);
        attach.isRead = false;
        attach.mainThread = Thread.currentThread();

        Charset cs = Charset.forName("UTF-8");
        String msg = "{\"CMD\":\"Get Steps\",\"Data\":0}*";
        byte[] data = msg.getBytes(cs);
        attach.buffer.put(data);
        attach.buffer.flip();

        XScanConfigHandler readWriteHandler = new XScanConfigHandler();

        //***********************************************************************//
        //Gui


        JFrame mainFrame = new JFrame("xSCAN Conbobulator");
        mainFrame.setSize(400, 400);
        mainFrame.setLocation(0, 400);
        mainFrame.setLayout(new GridBagLayout());
        GridBagConstraints fgbc = new GridBagConstraints();
        fgbc.weightx = 0.5;
        fgbc.weighty = 0.5;

        JPanel channelPanel = new JPanel();
        //channelPanel.setPreferredSize(mainFrame.getSize());
        channelPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        gbc.weightx = 0.1;


        //PRF Setting
        JLabel prfLabel = new JLabel("PRF");
        JTextField prfText = new JTextField("PRF");
        SpinnerNumberModel sm = new SpinnerNumberModel(50, 0, 100000, 10);

        JSpinner setPRF = new JSpinner(sm);

        prfLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;

        channelPanel.add(prfLabel, gbc);

        setPRF.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                String msg = jsoner.jsonCreator("Set PRF", (int) setPRF.getValue());
                System.out.println(msg);
                readWriteHandler.setMessage(msg);

            }
        });
        setPRF.setValue(20);
        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridwidth = 1;
        channelPanel.add(setPRF, gbc);

        //Voltage
        JLabel voltageLabel = new JLabel("Voltage");
        voltageLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.anchor = GridBagConstraints.CENTER;
        channelPanel.add(voltageLabel, gbc);
        String[] voltages = {"0ff", "56V", "90V"};
        JComboBox setVoltage = new JComboBox<String>(voltages);
        setVoltage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String val = (String) setVoltage.getSelectedItem();
                int volt = 0;
                if (val.equals("90V")) {
                    volt = 2;
                } else if (val.equals("56V")) {
                    volt = 1;
                } else {
                    volt = 0;
                }
                String msg = jsoner.jsonCreator("Set Voltage", volt);
                System.out.println(msg);
                readWriteHandler.setMessage(msg);
            }
        });

        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        channelPanel.add(setVoltage, gbc);
        // Filters
        String[] filters = {"0.7-30MHz", "0.25-1MHz", "0.75-2MHz", "1.75-4MHz", "2.5-5MHz", "4.5-9MHz", "6-12MHz", "10-20MHz"};
        JComboBox preAmpFilter = new JComboBox<String>(filters);
        JComboBox postAmpFilter = new JComboBox<String>(filters);
        // Preamp Filter
        gbc.anchor = GridBagConstraints.CENTER;
        JLabel preAmpFilterLabel = new JLabel("PreAmpFilter");
        preAmpFilterLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        channelPanel.add(preAmpFilterLabel, gbc);
        preAmpFilter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int[] filters = {1, 1};
                String[] labels = {"Pre-Amp Filter", "Post-Amp Filter"};
                int result = preAmpFilter.getSelectedIndex();
                int val = 1;
                switch (result) {
                    case 0:
                        val = 1;
                        break;
                    case 1:
                        val = 2;
                        break;
                    case 2:
                        val = 4;
                        break;
                    case 3:
                        val = 8;
                        break;
                    case 4:
                        val = 16;
                        break;
                    case 5:
                        val = 32;
                        break;
                    case 6:
                        val = 64;
                        break;
                    case 7:
                        val = 128;
                        break;
                }
                filters[0] = val;
                result = postAmpFilter.getSelectedIndex();
                val = 1;
                switch (result) {
                    case 0:
                        val = 1;
                        break;
                    case 1:
                        val = 2;
                        break;
                    case 2:
                        val = 4;
                        break;
                    case 3:
                        val = 8;
                        break;
                    case 4:
                        val = 16;
                        break;
                    case 5:
                        val = 32;
                        break;
                    case 6:
                        val = 64;
                        break;
                    case 7:
                        val = 128;
                        break;
                }
                filters[1] = val;
                String msg = jsoner.jsonCreator("Set Filters", labels, filters);
                System.out.println(msg);
                readWriteHandler.setMessage(msg);


            }
        });

        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        channelPanel.add(preAmpFilter, gbc);
        //Postamp Filter
        gbc.anchor = GridBagConstraints.CENTER;
        JLabel postAmpFilterLabel = new JLabel("PostAmpFilter");
        postAmpFilterLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        channelPanel.add(postAmpFilterLabel, gbc);

        postAmpFilter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int[] filters = {1, 1};
                String[] labels = {"Pre-Amp Filter", "Post-Amp Filter"};
                int result = preAmpFilter.getSelectedIndex();
                int val = 1;
                switch (result) {
                    case 0:
                        val = 1;
                        break;
                    case 1:
                        val = 2;
                        break;
                    case 2:
                        val = 4;
                        break;
                    case 3:
                        val = 8;
                        break;
                    case 4:
                        val = 16;
                        break;
                    case 5:
                        val = 32;
                        break;
                    case 6:
                        val = 64;
                        break;
                    case 7:
                        val = 128;
                        break;
                }
                filters[0] = val;
                result = postAmpFilter.getSelectedIndex();
                val = 1;
                switch (result) {
                    case 0:
                        val = 1;
                        break;
                    case 1:
                        val = 2;
                        break;
                    case 2:
                        val = 4;
                        break;
                    case 3:
                        val = 8;
                        break;
                    case 4:
                        val = 16;
                        break;
                    case 5:
                        val = 32;
                        break;
                    case 6:
                        val = 64;
                        break;
                    case 7:
                        val = 128;
                        break;
                }
                filters[1] = val;
                String msg = jsoner.jsonCreator("Set Filters", labels, filters);
                System.out.println(msg);
                readWriteHandler.setMessage(msg);

            }
        });
        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        channelPanel.add(postAmpFilter, gbc);
        //TXRX Channel
        String[] txrxList = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16"};

        JLabel txLabel = new JLabel("TX");
        txLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.anchor = GridBagConstraints.CENTER;
        channelPanel.add(txLabel, gbc);
        JComboBox setTX = new JComboBox(txrxList);
        setTX.setSelectedIndex(1);
        setTX.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int[] val = {setTX.getSelectedIndex()};

                String msg = jsoner.jsonCreator("Set TX Channels", val);
                System.out.println(msg);
                readWriteHandler.setMessage(msg);
            }
        });
        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        channelPanel.add(setTX, gbc);

        JLabel rxLabel = new JLabel("RX");
        txLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.anchor = GridBagConstraints.CENTER;
        channelPanel.add(rxLabel, gbc);
        JComboBox setRX = new JComboBox(txrxList);
        setRX.setSelectedIndex(1);
        setRX.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int[] val = {setRX.getSelectedIndex()};

                String msg = jsoner.jsonCreator("Set RX Channels", val);
                System.out.println(msg);
                readWriteHandler.setMessage(msg);
            }
        });
        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        channelPanel.add(setRX, gbc);
        //PulseType
        String[] pulseTypeList = {"Positive Squarewave", "Negative Squarewave", "Bipolar Squarewave(Pos-Neg)", "Bipolar Squarewave(Neg-Pos)", "Positive Pulse Train", "Negative Pulse Train", "Bipolar Pulse Train(Pos-Neg)", "Bipolar Pulse Train(Neg-Pos)"};

        JLabel pulseTrainLabel = new JLabel("Iterations 0-25");
        SpinnerNumberModel spti = new SpinnerNumberModel(1, 0, 25, 1);
        JSpinner setPulseTrainIterations = new JSpinner(spti);

        JLabel pulseLabel = new JLabel("PulseType");
        pulseLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.anchor = GridBagConstraints.CENTER;
        channelPanel.add(pulseLabel, gbc);
        JComboBox setPulseType = new JComboBox(pulseTypeList);
        setPulseType.setSelectedIndex(1);
        setPulseType.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int[] val = {setPulseType.getSelectedIndex()};
                if (val[0] < 4) {
                    setPulseTrainIterations.setVisible(false);
                    pulseTrainLabel.setVisible(false);
                } else {
                    setPulseTrainIterations.setVisible(true);
                    pulseTrainLabel.setVisible(true);
                }
                String msg = jsoner.jsonCreator("Set Pulse Types", val);
                System.out.println(msg);
                readWriteHandler.setMessage(msg);
            }
        });
        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        channelPanel.add(setPulseType, gbc);

        pulseTrainLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.anchor = GridBagConstraints.CENTER;
        setPulseTrainIterations.setVisible(false);
        pulseTrainLabel.setVisible(false);
        channelPanel.add(pulseTrainLabel, gbc);


        setPulseTrainIterations.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int[] val = {(int) setPulseTrainIterations.getValue()};
                String msg = jsoner.jsonCreator("Set Pulse Train Iterations", val);
                System.out.println(msg);
                readWriteHandler.setMessage(msg);

            }
        });
        //setPulseTrainIterations.setMinimumSize(new Dimension(20,2));
        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;

        channelPanel.add(setPulseTrainIterations, gbc);
        //PulseWidth
        JLabel pwLabel = new JLabel("Pulse Width");

        SpinnerNumberModel pw = new SpinnerNumberModel(50, 0, 5000, 10);

        JSpinner setPW = new JSpinner(pw);

        pwLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.anchor = GridBagConstraints.CENTER;
        channelPanel.add(pwLabel, gbc);

        setPW.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int[] val = {(int) setPW.getValue()};
                String msg = jsoner.jsonCreator("Set Pulse Widths", val);
                System.out.println(msg);
                readWriteHandler.setMessage(msg);

            }
        });
        setPW.setValue(20);
        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridwidth = 1;
        channelPanel.add(setPW, gbc);
        //PulseClamp
        JLabel pClampLabel = new JLabel("Pulse Clamp");

        SpinnerNumberModel pClamp = new SpinnerNumberModel(50, 0, 5000, 10);

        JSpinner setpClamp = new JSpinner(pClamp);

        pClampLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.anchor = GridBagConstraints.CENTER;
        channelPanel.add(pClampLabel, gbc);

        setpClamp.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int[] val = {(int) setpClamp.getValue()};
                String msg = jsoner.jsonCreator("Set Pulse Clamps", val);
                System.out.println(msg);
                readWriteHandler.setMessage(msg);

            }
        });
        setpClamp.setValue(0);
        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridwidth = 1;
        channelPanel.add(setpClamp, gbc);
        //LNA

        //Range
        JLabel rageLabel = new JLabel("Range");

        SpinnerNumberModel rage = new SpinnerNumberModel(50, 0, 5000, 1);

        JSpinner setrage = new JSpinner(rage);

        rageLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.anchor = GridBagConstraints.CENTER;
        channelPanel.add(rageLabel, gbc);

        setrage.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int[] val = {(int) setrage.getValue()};
                String msg = jsoner.jsonCreator("Set Ranges", val);
                System.out.println(msg);
                readWriteHandler.setMessage(msg);

            }
        });
        setrage.setValue(20);
        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridwidth = 1;
        channelPanel.add(setrage, gbc);

        //Delay
        JLabel delayLabel = new JLabel("Delay");

        SpinnerNumberModel delay = new SpinnerNumberModel(50, 0, 5000, 1);

        JSpinner setdelay = new JSpinner(delay);

        delayLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.anchor = GridBagConstraints.CENTER;
        channelPanel.add(delayLabel, gbc);

        setdelay.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int[] val = {(int) setdelay.getValue()};
                String msg = jsoner.jsonCreator("Set Delays", val);
                System.out.println(msg);
                readWriteHandler.setMessage(msg);

            }
        });
        setdelay.setValue(0);
        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridwidth = 1;
        channelPanel.add(setdelay, gbc);
        //Gain
        JLabel gainLabel = new JLabel("Gain");
        double initial = 0.0;
        double minimum = 0.0;
        double maximum = 80.0;
        double increment = 0.1;

        SpinnerNumberModel gain = new SpinnerNumberModel(initial, minimum, maximum, increment);

        JSpinner setgain = new JSpinner(gain);

        gainLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.anchor = GridBagConstraints.CENTER;
        channelPanel.add(gainLabel, gbc);

        setgain.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                double[] val = {(double) setgain.getValue()};

//                val[0] = Math.exp(Math.log10(val[0]/80.0));
//                val[0] = (double) Math.round((val[0]*100.0))/100.0*80;
                System.out.println("dB = " + val[0]);

                String msg = jsoner.jsonCreator("Set Fast Gain", val);
                System.out.println(msg);
                readWriteHandler.setMessage(msg);

            }
        });
        setgain.setValue(0.0);
        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridwidth = 1;
        channelPanel.add(setgain, gbc);
        //LNA
        JLabel lnaLabel = new JLabel("Low Noise Amplifier");
        lnaLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.anchor = GridBagConstraints.CENTER;
        channelPanel.add(lnaLabel, gbc);
        String[] lnas = {"LNA On", "LNA Off"};
        JComboBox setlna = new JComboBox<String>(lnas);
        setlna.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String val = (String) setlna.getSelectedItem();
                boolean[] value = {false};
                if (val.equals("LNA On")) {
                    value[0] = true;
                } else {
                    value[0] = false;
                }
                String msg = jsoner.jsonCreator("Set LNAs", value);
                System.out.println(msg);
                readWriteHandler.setMessage(msg);
            }
        });

        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        channelPanel.add(setlna, gbc);
        //StartStream
        String[] startStreamNames = {"Metadata", "Range (samples)", "Delay (samples)", "ADC Data", "Gate 1", "Gate 2", "Gate 3", "Constant Gain", "Spare 2", "Spare 3", "Spare 4", "Spare 5", "Spare 6", "Spare 7", "Spare 8"};
        Boolean[] startStreamData = {true, true, true, true, false, false, false, false, false, false, false, false, false, false, false};
        JButton startStream = new JButton("Start Stream");
        startStream.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String st = jsoner.jsonCreator("Set Data to Send", startStreamNames, startStreamData);
                System.out.println(st);
                readWriteHandler.setMessage(st);
            }
        });
        gbc.gridy += 1;
        gbc.gridx = 1;

        channelPanel.add(startStream, gbc);

        JButton channelTitle = new JButton("Step 01");
        channelTitle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                channelPanel.setVisible(!channelPanel.isVisible());
            }
        });
        JPanel channelContainer = new JPanel();
        channelContainer.setLayout(new GridBagLayout());
        GridBagConstraints cC = new GridBagConstraints();
        cC.anchor = GridBagConstraints.NORTH;
        cC.fill = GridBagConstraints.HORIZONTAL;
        cC.gridx = 0;
        channelContainer.add(channelTitle,cC);
        cC.gridy = 1;
        channelContainer.add(channelPanel,cC);
        fgbc.weightx = 0.1;
        fgbc.gridx = 0;
        fgbc.gridy = 1;
        fgbc.gridwidth = 1;
        fgbc.fill = GridBagConstraints.HORIZONTAL;

        fgbc.anchor = GridBagConstraints.NORTH;
        mainFrame.add(channelContainer, fgbc);


        mainFrame.setVisible(true);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        //***********************************************************************//
        //Listen for change to currentConfiguration
        PropertyChangeListener listen = new PropertyChangeListener() {


            @Override
            public void propertyChange(PropertyChangeEvent evt) {

                //currentConfig = (String) evt.getNewValue();
                System.out.println("propertychange triggered");

            }
        };
        readWriteHandler.support.addPropertyChangeListener(listen);



        //KeepAlive*******************************************************************//

        class keepAlive extends TimerTask {
            @Override
            public void run() {
                System.out.println("timer");
                jsonHandler msg = new jsonHandler();
                String json = msg.jsonCreator("Get Time ISO8601", 0) + "*\n";
                readWriteHandler.setMessage(json);
            }
        }
        Timer timer = new Timer();
        TimerTask keepUp = new keepAlive();
        timer.schedule(keepUp, 1000, 5000);

        //end KeepAlive

        channel.write(attach.buffer, attach, readWriteHandler);
        attach.mainThread.join();


    }
}

