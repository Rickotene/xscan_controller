import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class XScanHeadlessDataStreamHandler {
    public static void main(String[] args) throws IOException, InterruptedException {
        PipedOutputStream pipedOutputStream = new PipedOutputStream();
        PipedInputStream pipedInputStream = new PipedInputStream(pipedOutputStream);

        XScanDataStreamHandler stream = new XScanDataStreamHandler(pipedOutputStream);
        XScanInputStreamParser parser = new XScanInputStreamParser(pipedInputStream,false);

        Thread sThread = new Thread(stream);
        Thread pThread = new Thread(parser);

        sThread.start();
        pThread.start();

        sThread.join();
        pThread.join();
        pipedOutputStream.close();
        pipedInputStream.close();
    }
}


