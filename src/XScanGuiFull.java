import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.json.JSONObject;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;


public class XScanGuiFull {

    jsonHandler jsoner = new jsonHandler();

    String commandToSend = "";

    String configMessage = "";

    PropertyChangeSupport cmd = new PropertyChangeSupport(commandToSend);

    JFreeChart aSCAN;
    JFreeChart aSCANZoom;

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        cmd.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        cmd.removePropertyChangeListener(listener);
    }

    public void setConfig(String conf){

        configMessage = conf;
        System.out.println("configMessage:\n"+configMessage);
        parseConfig();


    }

    public void parseConfig() {

        //******************NEED TO SORT
        // CONFIG ARRAY AND FOR LOOP FOR HANDLING MISS PARSED CONFIG MESSAGES
        String[] configArray = configMessage.split("\\*");
        for (int j = 0; j < configArray.length; j++) {
            config = new JSONObject(configArray[j]);

            //System.out.format("Server Responded: " + config + "\n");

            String command = config.getString("CMD");

            if (!isGlobalsSet) {
                if (command.equals("Steps")) {
                    org.json.JSONArray steps = config.getJSONArray("Data");

                    for (int i = 0; i < steps.length(); i++) {

                        tx[i] = steps.getJSONObject(i).getInt("TX Channel");
                        System.out.println("TX Channel " + i + ": " + tx[i]);

                        rx[i] = steps.getJSONObject(i).getInt("RX Channel");
                        System.out.println("RX Channel " + i + ": " + rx[i]);

                        pt[i] = steps.getJSONObject(i).getInt("Pulse Type");
                        System.out.println("Pulse Type " + i + ": " + pt[i]);

                        pti[i] = steps.getJSONObject(i).getInt("Pulse Train Iterations");
                        System.out.println("Pulse Train Iterations " + i + ": " + tx[i]);

                        pw[i] = steps.getJSONObject(i).getInt("Pulse Width (ns)");
                        System.out.println("Pulse Widths (ns) " + i + ": " + pw[i]);

                        c[i] = steps.getJSONObject(i).getInt("Pulse Clamp (ns)");
                        System.out.println("Pulse Clamp (ns) " + i + ": " + c[i]);

                        r[i] = steps.getJSONObject(i).getDouble("Range (us)");
                        System.out.println("Range (us) " + i + ": " + r[i]);

                        d[i] = steps.getJSONObject(i).getDouble("Delay (us)");
                        System.out.println("Delay (us) " + i + ": " + d[i]);

                        g[i] = steps.getJSONObject(i).getDouble("Fast Gain (dB)");
                        System.out.println("Fast Gain (dB) " + i + ": " + g[i]);

                        lna[i] = steps.getJSONObject(i).getBoolean("LNA");
                        System.out.println("LNA " + i + ": " + tx[i]);

                        // Add TOF gates here


                    }

                    isChannelsSet = true;

                    //get PRF
                    String msg = jsoner.jsonCreator("Get PRF", 0);
                    //System.out.println(msg);
                    setCommandToSend(msg);

                }

                if (command.equals("PRF")) {
                    prf = config.getInt("Data");
                    String msg = jsoner.jsonCreator("Get Voltage", 0);
                    //System.out.println(msg);
                    setCommandToSend(msg);
                }

                if (command.equals("Voltage")) {
                    voltage = config.getInt("Data");
                    String msg = jsoner.jsonCreator("Get Filters", 0);
                    //System.out.println(msg);
                    setCommandToSend(msg);
                }

                if (command.equals("Filters")) {
                    preFilter = config.getJSONObject("Data").getInt("Pre-Amp Filter");
                    System.out.println(preFilter);
                    postFilter = config.getJSONObject("Data").getInt("Post-Amp Filter");
                    System.out.println(postFilter);
                    String msg = jsoner.jsonCreator("Get Step Count", 0);
                    //System.out.println(msg);
                    setCommandToSend(msg);
                }

                if (command.equals("Step Count")) {
                    steps = config.getInt("Data");
                    isGlobalsSet = true;
                    buildGui();

                    String msg = jsoner.jsonCreator("Set Data to Send", startStreamNames, startStreamData);
                    //System.out.println(msg);
                    setCommandToSend(msg);
                }

            }

            if (command.equals("xSCAN Error")) {
                JSONObject error = config.getJSONObject("Data");
                String status = "Status: " + error.getBoolean("status") + "\n";
                String code = "Code: " + error.getInt("code")+"\n";
                String source = error.getString("source");
                JOptionPane.showMessageDialog(null, status+code+source, "xSCAN Error", JOptionPane.WARNING_MESSAGE);

            }
//        String msg = jsoner.jsonCreator("Set Delays", d);
//        //System.out.println(msg);
//        setCommandToSend(msg);

        }
    }

    public void setCommandToSend(String m){
        String oldValue = commandToSend;
        System.out.println("Shots Fired");
        cmd.firePropertyChange("message",oldValue, m);
    }


    public JFrame frame = new JFrame();

    JPanel globalPanel;
    DepthButton globalTitle;
    JPanel globalContainer = new JPanel();

    JPanel[] channels = new JPanel[16];
    DepthButton[] channelTitle = new DepthButton[16];
    JPanel configPanel = new JPanel();

    JPanel waveformPanel = new JPanel();


    //Configuration Variables

    org.json.JSONObject config;

    //Globals
    boolean isGlobalsSet = false;
    int prf = 0;
    int voltage = 0;
    int preFilter = 0;
    int postFilter = 0;
    int steps = 1;

    //Steps
    boolean isChannelsSet = false;
    int[] tx = new int[16];
    int[] rx = new int[16];
    int[] pt = new int[16];
    int[] pti = new int[16];
    int[] pw = new int[16];
    int[] c = new int[16];
    double[] r = new double[16];
    double[] d = new double[16];
    double[] g = new double[16];
    boolean[] lna = new boolean[16];

    //Data to stream
    String[] startStreamNames = {"Metadata", "Range (samples)", "Delay (samples)", "ADC Data", "Gate 1", "Gate 2", "Gate 3", "Constant Gain", "Spare 2", "Spare 3", "Spare 4", "Spare 5", "Spare 6", "Spare 7", "Spare 8"};
    Boolean[] startStreamData = {true, true, true, true, false, false, false, false, false, false, false, false, false, false, false};

    //waveform variables
    ArrayList<Integer> data = new ArrayList<Integer>();
    JSlider delaySlider = new JSlider(0, 0, 0);
    JSlider rangeSlider = new JSlider(0, 0);

    public JPanel channelBuilder(int channelNo) {

        JPanel channelPanel;


        channelPanel = new JPanel();

        channelPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        gbc.weightx = 0.1;


        channelTitle[channelNo] = new DepthButton("Step " + Integer.toString(channelNo),channelNo);
        channelTitle[channelNo].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                channelPanel.setVisible(!channelPanel.isVisible());
            }
        });
        channelPanel.setVisible(false);

        //TXRX Channel
        String[] txrxList = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16"};

        JLabel txLabel = new JLabel("TX");
        txLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.anchor = GridBagConstraints.CENTER;
        channelPanel.add(txLabel, gbc);
        JComboBox setTX = new JComboBox(txrxList);
        setTX.setSelectedIndex(tx[channelNo]);
        setTX.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                tx[channelNo] = setTX.getSelectedIndex();

                String msg = jsoner.jsonCreator("Set TX Channels", tx);
                //System.out.println(msg);
                setCommandToSend(msg);
            }
        });
        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        channelPanel.add(setTX, gbc);

        JLabel rxLabel = new JLabel("RX");
        txLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.anchor = GridBagConstraints.CENTER;
        channelPanel.add(rxLabel, gbc);
        JComboBox setRX = new JComboBox(txrxList);
        setRX.setSelectedIndex(rx[channelNo]);
        setRX.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                rx[channelNo] = setRX.getSelectedIndex();

                String msg = jsoner.jsonCreator("Set RX Channels", rx);
                //System.out.println(msg);
                setCommandToSend(msg);
            }
        });
        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        channelPanel.add(setRX, gbc);

        //PulseType
        String[] pulseTypeList = {"Positive Squarewave", "Negative Squarewave", "Bipolar Squarewave(Pos-Neg)", "Bipolar Squarewave(Neg-Pos)", "Positive Pulse Train", "Negative Pulse Train", "Bipolar Pulse Train(Pos-Neg)", "Bipolar Pulse Train(Neg-Pos)"};

        JLabel pulseTrainLabel = new JLabel("Iterations 0-25");
        SpinnerNumberModel spti = new SpinnerNumberModel(pti[channelNo], 0, 25, 1);
        JSpinner setPulseTrainIterations = new JSpinner(spti);

        JLabel pulseLabel = new JLabel("PulseType");
        pulseLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.anchor = GridBagConstraints.CENTER;
        channelPanel.add(pulseLabel, gbc);
        JComboBox setPulseType = new JComboBox(pulseTypeList);
        setPulseType.setSelectedIndex(pt[channelNo]);
        setPulseType.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pt[channelNo] = setPulseType.getSelectedIndex();
                if (pt[0] < 4) {
                    setPulseTrainIterations.setVisible(false);
                    pulseTrainLabel.setVisible(false);
                } else {
                    setPulseTrainIterations.setVisible(true);
                    pulseTrainLabel.setVisible(true);
                }
                String msg = jsoner.jsonCreator("Set Pulse Types", pt);
                //System.out.println(msg);
                setCommandToSend(msg);
            }
        });
        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        channelPanel.add(setPulseType, gbc);

        pulseTrainLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.anchor = GridBagConstraints.CENTER;
        setPulseTrainIterations.setVisible(false);
        pulseTrainLabel.setVisible(false);
        channelPanel.add(pulseTrainLabel, gbc);


        setPulseTrainIterations.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                pti[channelNo] =(int) setPulseTrainIterations.getValue();
                String msg = jsoner.jsonCreator("Set Pulse Train Iterations", pti);
                //System.out.println(msg);
                setCommandToSend(msg);

            }
        });

        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;

        channelPanel.add(setPulseTrainIterations, gbc);

        //PulseWidth
        JLabel pwLabel = new JLabel("Pulse Width");

        SpinnerNumberModel pulseWidths = new SpinnerNumberModel(pw[channelNo], 0, 5000, 10);

        JSpinner setPW = new JSpinner(pulseWidths);

        pwLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.anchor = GridBagConstraints.CENTER;
        channelPanel.add(pwLabel, gbc);

        setPW.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                pw[channelNo] = (int) setPW.getValue();
                String msg = jsoner.jsonCreator("Set Pulse Widths", pw);
                //System.out.println(msg);
                setCommandToSend(msg);

            }
        });

        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridwidth = 1;
        channelPanel.add(setPW, gbc);

        //PulseClamp
        JLabel pClampLabel = new JLabel("Pulse Clamp");

        SpinnerNumberModel pClamp = new SpinnerNumberModel(c[channelNo], 0, 5000, 10);

        JSpinner setpClamp = new JSpinner(pClamp);

        pClampLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.anchor = GridBagConstraints.CENTER;
        channelPanel.add(pClampLabel, gbc);

        setpClamp.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                c[channelNo] = (int) setpClamp.getValue();
                String msg = jsoner.jsonCreator("Set Pulse Clamps", c);
                //System.out.println(msg);
                setCommandToSend(msg);

            }
        });

        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridwidth = 1;
        channelPanel.add(setpClamp, gbc);

        //Range
        JLabel rangeLabel = new JLabel("Range");

        double minimum = 0.0;
        double maximum = 5000.0;
        double increment = 0.1;
        SpinnerNumberModel range = new SpinnerNumberModel(r[channelNo], minimum, maximum, increment);

        JSpinner setRange = new JSpinner(range);

        rangeLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.anchor = GridBagConstraints.CENTER;
        channelPanel.add(rangeLabel, gbc);

        setRange.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                r[channelNo] = (double) setRange.getValue();
                String msg = jsoner.jsonCreator("Set Ranges", r);
                System.out.println(msg);
                setCommandToSend(msg);

            }
        });

        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridwidth = 1;
        channelPanel.add(setRange, gbc);

        //Delay
        JLabel delayLabel = new JLabel("Delay");
        minimum = 0.0;
        maximum = 5000.0;
        increment = 0.1;
        SpinnerNumberModel delay = new SpinnerNumberModel(d[channelNo], minimum, maximum, increment);

        JSpinner setdelay = new JSpinner(delay);

        delayLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.anchor = GridBagConstraints.CENTER;
        channelPanel.add(delayLabel, gbc);

        setdelay.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                d[channelNo] =(double) setdelay.getValue();
                String msg = jsoner.jsonCreator("Set Delays", d);
                //System.out.println(msg);
                setCommandToSend(msg);

            }
        });

        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridwidth = 1;
        channelPanel.add(setdelay, gbc);

        //Gain
        JLabel gainLabel = new JLabel("Gain");

        minimum = 0.0;
        maximum = 80.0;
        increment = 0.1;

        SpinnerNumberModel gain = new SpinnerNumberModel(g[channelNo], minimum, maximum, increment);

        JSpinner setgain = new JSpinner(gain);

        gainLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.anchor = GridBagConstraints.CENTER;
        channelPanel.add(gainLabel, gbc);

        setgain.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                g[channelNo] = (double) setgain.getValue();



                String msg = jsoner.jsonCreator("Set Fast Gain", g);
                //System.out.println(msg);
                setCommandToSend(msg);

            }
        });

        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridwidth = 1;
        channelPanel.add(setgain, gbc);

        //LNA
        JLabel lnaLabel = new JLabel("Low Noise Amplifier");
        lnaLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.anchor = GridBagConstraints.CENTER;
        channelPanel.add(lnaLabel, gbc);
        String[] lnas = {"LNA On", "LNA Off"};
        JComboBox setlna = new JComboBox<String>(lnas);

        if(lna[channelNo]) {
            setlna.setSelectedIndex(0);
        }
        else{
            setlna.setSelectedIndex(1);
        }

        setlna.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String val = (String) setlna.getSelectedItem();

                if (val.equals("LNA On")) {
                    lna[channelNo] = true;
                } else {
                    lna[channelNo] = false;
                }
                String msg = jsoner.jsonCreator("Set LNAs", lna);
                System.out.println(msg);
                setCommandToSend(msg);
            }
        });

        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        channelPanel.add(setlna, gbc);

        return channelPanel;
    }

    public void buildGlobals() {

        globalContainer.setLayout(new GridBagLayout());
        GridBagConstraints gpc = new GridBagConstraints();
        gpc.gridx = 0;
        gpc.gridy = 0;
        gpc.weightx = 0.5;
        gpc.weighty = 0.5;
        gpc.gridwidth = 2;
        gpc.fill = GridBagConstraints.HORIZONTAL;

        globalPanel = new JPanel();
        //globalPanel.setPreferredSize(mainFrame.getSize());
        globalPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        gbc.weightx = 0.1;


        //PRF Setting
        JLabel prfLabel = new JLabel("PRF");
        JTextField prfText = new JTextField("PRF");
        SpinnerNumberModel sm = new SpinnerNumberModel(prf, 0, 100000, 10);

        JSpinner setPRF = new JSpinner(sm);

        prfLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;

        globalPanel.add(prfLabel, gbc);

        setPRF.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                String msg = jsoner.jsonCreator("Set PRF", (int) setPRF.getValue());
                //System.out.println(msg);
                setCommandToSend(msg);

            }
        });

        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridwidth = 1;
        globalPanel.add(setPRF, gbc);
        //Voltage
        JLabel voltageLabel = new JLabel("Voltage");
        voltageLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        gbc.anchor = GridBagConstraints.CENTER;
        globalPanel.add(voltageLabel, gbc);
        String[] voltages = {"0ff", "56V", "90V"};
        JComboBox setVoltage = new JComboBox<String>(voltages);
        setVoltage.setSelectedIndex(voltage);
        setVoltage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String val = (String) setVoltage.getSelectedItem();
                int volt = 0;
                if (val.equals("90V")) {
                    volt = 2;
                } else if (val.equals("56V")) {
                    volt = 1;
                } else {
                    volt = 0;
                }
                String msg = jsoner.jsonCreator("Set Voltage", volt);
                System.out.println(msg);
               setCommandToSend(msg);
            }
        });

        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        globalPanel.add(setVoltage, gbc);
        // Filters
        String[] filters = {"0.7-30MHz", "0.25-1MHz", "0.75-2MHz", "1.75-4MHz", "2.5-5MHz", "4.5-9MHz", "6-12MHz", "10-20MHz"};
        JComboBox preAmpFilter = new JComboBox<String>(filters);
        JComboBox postAmpFilter = new JComboBox<String>(filters);
        // Preamp Filter
        gbc.anchor = GridBagConstraints.CENTER;
        JLabel preAmpFilterLabel = new JLabel("PreAmpFilter");
        preAmpFilterLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;

        int val = 0;
        switch (preFilter) {
            case 1:
                val = 0;
                break;
            case 2:
                val = 1;
                break;
            case 4:
                val = 2;
                break;
            case 8:
                val = 3;
                break;
            case 16:
                val = 4;
                break;
            case 32:
                val = 5;
                break;
            case 64:
                val = 6;
                break;
            case 128:
                val = 7;
                break;
        }
        preAmpFilter.setSelectedIndex(val);
        switch (postFilter) {
            case 1:
                val = 0;
                break;
            case 2:
                val = 1;
                break;
            case 4:
                val = 2;
                break;
            case 8:
                val = 3;
                break;
            case 16:
                val = 4;
                break;
            case 32:
                val = 5;
                break;
            case 64:
                val = 6;
                break;
            case 128:
                val = 7;
                break;
        }
        postAmpFilter.setSelectedIndex(val);

        globalPanel.add(preAmpFilterLabel, gbc);
        preAmpFilter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int[] filters = {1, 1};
                String[] labels = {"Pre-Amp Filter", "Post-Amp Filter"};
                int result = preAmpFilter.getSelectedIndex();
                int val = 1;
                switch (result) {
                    case 0:
                        val = 1;
                        break;
                    case 1:
                        val = 2;
                        break;
                    case 2:
                        val = 4;
                        break;
                    case 3:
                        val = 8;
                        break;
                    case 4:
                        val = 16;
                        break;
                    case 5:
                        val = 32;
                        break;
                    case 6:
                        val = 64;
                        break;
                    case 7:
                        val = 128;
                        break;
                }
                filters[0] = val;
                result = postAmpFilter.getSelectedIndex();
                val = 1;
                switch (result) {
                    case 0:
                        val = 1;
                        break;
                    case 1:
                        val = 2;
                        break;
                    case 2:
                        val = 4;
                        break;
                    case 3:
                        val = 8;
                        break;
                    case 4:
                        val = 16;
                        break;
                    case 5:
                        val = 32;
                        break;
                    case 6:
                        val = 64;
                        break;
                    case 7:
                        val = 128;
                        break;
                }
                filters[1] = val;
                String msg = jsoner.jsonCreator("Set Filters", labels, filters);
                //System.out.println(msg);
                setCommandToSend(msg);


            }
        });



        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        globalPanel.add(preAmpFilter, gbc);
        //Postamp Filter
        gbc.anchor = GridBagConstraints.CENTER;
        JLabel postAmpFilterLabel = new JLabel("PostAmpFilter");
        postAmpFilterLabel.setHorizontalAlignment(SwingConstants.CENTER);
        gbc.gridx = 0;
        gbc.gridy += 1;
        globalPanel.add(postAmpFilterLabel, gbc);

        postAmpFilter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int[] filters = {1, 1};
                String[] labels = {"Pre-Amp Filter", "Post-Amp Filter"};
                int result = preAmpFilter.getSelectedIndex();
                int val = 1;
                switch (result) {
                    case 0:
                        val = 1;
                        break;
                    case 1:
                        val = 2;
                        break;
                    case 2:
                        val = 4;
                        break;
                    case 3:
                        val = 8;
                        break;
                    case 4:
                        val = 16;
                        break;
                    case 5:
                        val = 32;
                        break;
                    case 6:
                        val = 64;
                        break;
                    case 7:
                        val = 128;
                        break;
                }
                filters[0] = val;
                result = postAmpFilter.getSelectedIndex();
                val = 1;
                switch (result) {
                    case 0:
                        val = 1;
                        break;
                    case 1:
                        val = 2;
                        break;
                    case 2:
                        val = 4;
                        break;
                    case 3:
                        val = 8;
                        break;
                    case 4:
                        val = 16;
                        break;
                    case 5:
                        val = 32;
                        break;
                    case 6:
                        val = 64;
                        break;
                    case 7:
                        val = 128;
                        break;
                }
                filters[1] = val;
                String msg = jsoner.jsonCreator("Set Filters", labels, filters);
                System.out.println(msg);
                setCommandToSend(msg);

            }
        });
        gbc.gridx = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        globalPanel.add(postAmpFilter, gbc);

        globalTitle = new DepthButton("Global Variables",16);
        globalTitle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                globalPanel.setVisible(!globalPanel.isVisible());
            }
        });
        //StartStream
        //Data to stream

//
//        DepthButton startStream = new DepthButton("Start Stream",0);
//        startStream.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                String msg = jsoner.jsonCreator("Set Data to Send", startStreamNames, startStreamData);
//                System.out.println(msg);
//                setCommandToSend(msg);
//            }
//        });
//
//        gbc.gridy += 1;
//        gbc.gridx = 1;
//        globalPanel.add(startStream, gbc);

        gbc.gridy+=1;
        gbc.gridx = 0;
        gbc.gridwidth = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.fill = GridBagConstraints.NONE;
        JLabel stepsLabel = new JLabel("StepCount");
        globalPanel.add(stepsLabel,gbc);
        gbc.gridx = 1;
        SpinnerNumberModel sc = new SpinnerNumberModel(steps,1,16,1);
        JSpinner stepCount = new JSpinner(sc);
        stepCount.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int val =  (int)stepCount.getValue();
                String msg = jsoner.jsonCreator("Set Step Count", val);
                System.out.println(msg);
                setCommandToSend(msg);

                for(int i = 0; i < channels.length;i ++){
                    if(i<=(int)stepCount.getValue()-1){
                       channelTitle[i].setEnabled(true);

                    }
                    else{
                        channelTitle[i].setEnabled(false);
                        channels[i].setVisible(false);
                    }
                }
            }
        });

        gbc.anchor = GridBagConstraints.WEST;
        globalPanel.add((stepCount),gbc);

//        String[] startStreamNames = {"Metadata", "Range (samples)", "Delay (samples)", "ADC Data", "Gate 1", "Gate 2", "Gate 3", "Constant Gain", "PeakToPeak", "Bandwidth", "Spare 4", "Spare 5", "Spare 6", "Spare 7", "Spare 8"};
//        Boolean[] startStreamData = {true, true, true, true, false, false, false, false, false, false, false, false, false, false, false};


//        // datatosend check boxes

        JCheckBox enableADC = new JCheckBox("Enable ADC Data");
        enableADC.setSelected(startStreamData[3]);
        enableADC.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    if(startStreamData[3]!=enableADC.isSelected()) {
                        startStreamData[3] = enableADC.isSelected();
                        String msg = jsoner.jsonCreator("Set Data to Send", startStreamNames, startStreamData);
                        setCommandToSend(msg);
                    }
                }
        });

        gbc.gridx = 0;
        gbc.gridy += 1;
        globalPanel.add(enableADC,gbc);

        JCheckBox enableTOF1 = new JCheckBox("Enable TOF 1");
        enableTOF1.setSelected(startStreamData[4]);
        enableTOF1.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if(startStreamData[4]!= enableTOF1.isSelected()) {
                    startStreamData[4] = enableTOF1.isSelected();
                    String msg = jsoner.jsonCreator("Set Data to Send", startStreamNames, startStreamData);
                    setCommandToSend(msg);
                }
            }
        });
        gbc.gridx = 0;
        gbc.gridy += 1;
        globalPanel.add(enableTOF1,gbc);

        JCheckBox enableTOF2 = new JCheckBox("Enable TOF 2");
        enableTOF2.setSelected(startStreamData[5]);
        enableTOF2.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if(startStreamData[5] != enableTOF1.isSelected()) {
                    startStreamData[5] = enableTOF1.isSelected();
                    String msg = jsoner.jsonCreator("Set Data to Send", startStreamNames, startStreamData);
                    setCommandToSend(msg);
                }
            }
        });
        gbc.gridx = 0;
        gbc.gridy += 1;
        globalPanel.add(enableTOF2,gbc);


        JCheckBox enableTOF3 = new JCheckBox("Enable TOF 3");
        enableTOF3.setSelected(startStreamData[6]);
        enableTOF3.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if(startStreamData[6] != enableTOF1.isSelected()) {
                    startStreamData[6] = enableTOF1.isSelected();
                    String msg = jsoner.jsonCreator("Set Data to Send", startStreamNames, startStreamData);
                    setCommandToSend(msg);
                }
            }
        });
        gbc.gridx = 0;
        gbc.gridy += 1;
        globalPanel.add(enableTOF3,gbc);

        JCheckBox enablePeakToPeak = new JCheckBox("Enable Peak to Peak");
        enablePeakToPeak.setSelected(startStreamData[8]);
        enablePeakToPeak.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if(startStreamData[8] != enableTOF1.isSelected()) {
                    startStreamData[8] = enableTOF1.isSelected();
                    String msg = jsoner.jsonCreator("Set Data to Send", startStreamNames, startStreamData);
                    setCommandToSend(msg);
                }
            }
        });
        gbc.gridx = 0;
        gbc.gridy += 1;
        globalPanel.add(enablePeakToPeak,gbc);








        gpc.gridy = 0;
        globalContainer.add(globalTitle,gpc);
        gpc.gridy +=1;
        globalContainer.add(globalPanel,gpc);




    }
    //end build global

    public XYDataset createDataset(ArrayList<Integer> data) {
        final XYSeries samples = new XYSeries("Step 0");

        for (int i = 0; i < data.size(); i++) {

            samples.add(i, data.get(i));
        }


        final XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(samples);
        return dataset;
    }

    public void buildWaveformpanel(){



        ArrayList<Integer> sampleData = new ArrayList<Integer>();
        sampleData.add(1);
        sampleData.add(2);

        aSCAN = ChartFactory.createXYLineChart(null, "Sample", "Amplitude", createDataset(sampleData), PlotOrientation.VERTICAL, false, true, false);
        aSCAN.getXYPlot().getRangeAxis().setRange(0, 16000);
        //aSCAN.getXYPlot().getDomainAxis().setRange(0,2000);
        ChartPanel panel = new ChartPanel(aSCAN);
        XYLineAndShapeRenderer renderer0 = new XYLineAndShapeRenderer(true,false);
        aSCAN.getXYPlot().setRenderer(0, renderer0);
        aSCAN.getXYPlot().getRendererForDataset(aSCAN.getXYPlot().getDataset(0)).setSeriesPaint(0, new Color(0x1E90FF));

        aSCANZoom = ChartFactory.createXYLineChart(null, "Sample", "Amplitude", createDataset(sampleData), PlotOrientation.VERTICAL, false, true, false);
        aSCANZoom.getXYPlot().getRangeAxis().setRange(0, 16000);
        //aSCAN.getXYPlot().getDomainAxis().setRange(0,2000);
        ChartPanel panelZoom = new ChartPanel(aSCANZoom);



        waveformPanel = new JPanel();
        waveformPanel.setLayout(new BoxLayout(waveformPanel, BoxLayout.Y_AXIS));

        waveformPanel.add(panel);
        waveformPanel.add(panelZoom);
        waveformPanel.add(delaySlider);
        waveformPanel.add(rangeSlider);

    }

    public void updateWaveform(ArrayList<Integer> raw){
        data = raw;
        rangeSlider.setMaximum((int)r[0]);
        if (rangeSlider.getValue() == 0) {
            rangeSlider.setValue((int)r[0]);
        }
        if (delaySlider.getMaximum() == 0) {
            delaySlider.setMaximum((int)r[0]);
        }
        int d = delaySlider.getValue();
        int r = rangeSlider.getValue();

        aSCAN.getXYPlot().getDomainAxis().setRange(0, r);
        aSCAN.getXYPlot().clearDomainMarkers();
        ValueMarker de = new ValueMarker(d);
        ValueMarker re = new ValueMarker(r);

        aSCAN.getXYPlot().addDomainMarker(de);
        aSCAN.getXYPlot().addDomainMarker(re);
        aSCANZoom.getXYPlot().getDomainAxis().setRange(delaySlider.getValue(), rangeSlider.getValue());
        XYDataset dataset = createDataset(data);
        aSCAN.getXYPlot().setDataset(dataset);
        aSCANZoom.getXYPlot().setDataset(dataset);

    }

    public void buildGui(){
        frame.setSize(1200, 800);
        frame.setLocation(600,0);


        configPanel.setLayout(new GridBagLayout());
        GridBagConstraints frm = new GridBagConstraints();




        JScrollPane scrollPane = new JScrollPane();

        frm.gridx = 0;
        frm.gridy = 0;
        frm.weightx = 0.5;
        frm.weighty = 1;
        frm.fill = GridBagConstraints.BOTH;
        frm.anchor = GridBagConstraints.NORTH;

        for (int i = 0; i < channels.length; i++) {

            channels[i] = channelBuilder(i);

        }


        for (int i = 0; i < channels.length; i++) {
            configPanel.add(channelTitle[i], frm);
            frm.gridy += 1;
            configPanel.add(channels[i], frm);
            frm.gridy += 1;

        }

        for(int i = 0; i < channels.length;i ++){
            if(i<=(int)steps-1){
                channelTitle[i].setEnabled(true);

            }
            else{
                channelTitle[i].setEnabled(false);
                channels[i].setVisible(false);
            }
        }

        buildGlobals();
        scrollPane.setColumnHeaderView(globalContainer);

        JViewport viewport = scrollPane.getViewport();
        viewport.setView(configPanel);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        buildWaveformpanel();

        waveformPanel.setPreferredSize(new Dimension(2048,2000));
        scrollPane.setPreferredSize(new Dimension(370,2000));

        waveformPanel.setMinimumSize(new Dimension(800,800));
        scrollPane.setMinimumSize(new Dimension(370,800));



        JSplitPane mainContainer = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,waveformPanel,scrollPane);
        mainContainer.setResizeWeight(1.0);
        //mainContainer.getRightComponent().setMaximumSize(new Dimension(370,2000));

        frame.getContentPane().add(mainContainer);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public XScanGuiFull() {

    //buildGui():

    }

    //for testing
    public static void main(String[] args){
        XScanGuiFull gui = new XScanGuiFull();
        gui.buildGui();
    }
}

