import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.PipedInputStream;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class XScanInputStreamParserTimer extends Thread{


    ArrayList<ArrayList<Integer>> data = new ArrayList<ArrayList<Integer>>();

    boolean isDebug = false;

    //header
    int header = 0;
    int stepNumber = 0;
    int rxChannel = 0;
    int txChannel = 0;

    //metaData
    int metaKey = 0;
    int range = 0;
    int delay = 0;
    boolean triggerState = false;
    int gate1TOF = 0;
    int gate2TOF = 0;
    int gate3TOF = 0;
    boolean constantCain = false;
    int peakToPeak = 0;
    int peakMin = 0;
    int peakMax = 0;

    SQLCommandsUpdate testBench;

    int saveCounter = 0;

    boolean update = false;
    PropertyChangeSupport updateAvailable = new PropertyChangeSupport(data);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        updateAvailable.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        updateAvailable.removePropertyChangeListener(listener);
    }

    //KeepAlive*******************************************************************//
    class UpdateTimer extends TimerTask {
        @Override
        public void run() {
            //Send notification to gui.
            if(update == true) {
                ArrayList<ArrayList<Integer>> d = data;
                updateAvailable.firePropertyChange("message", "here", d);
            }

        }
    }

    Timer updateTimer = new Timer();
    UpdateTimer updater = new UpdateTimer();

    public void startUpdateTimer(){
        updateTimer = new Timer();
        updater = new UpdateTimer();
        updateTimer.schedule(updater,0,50);
    }

    public void resetUpdateTimer(){
        updateTimer.cancel();
        updateTimer = new Timer();
        updater = new UpdateTimer();
        updateTimer.schedule(updater,0,50);
    }

    private final PipedInputStream inputStream;

    public XScanInputStreamParserTimer(PipedInputStream input, Boolean debug)  {

        this.inputStream = input;
        this.isDebug = debug;

    }

    private void sqlInserter(){

        if(saveCounter > 100){

            //demo setup
            testBench = new SQLCommandsUpdate();
            testBench.saveTOF(new int[]{stepNumber, rxChannel, txChannel, range, delay, gate1TOF, gate2TOF, gate3TOF, peakToPeak, peakMin, peakMax});

            saveCounter = 0;
        }
        else{
            saveCounter += 1;
        }


    }

    private void resetAll(){
        //header
        header = 0;
        stepNumber = 0;
        rxChannel = 0;
        txChannel = 0;

        //metaData
        metaKey = 0;
        range = 0;
        delay = 0;
        triggerState = false;
        gate1TOF = 0;
        gate2TOF = 0;
        gate3TOF = 0;
        constantCain = false;
        peakToPeak = 0;
        peakMin = 0;
        peakMax = 0;

    }
    @Override
    public void run(){
        for(int i = 0; i < 16; i++)  {
            data.add(new ArrayList<Integer>());
        }

        while (true) {

            boolean hasRange = false;
            boolean hasDelay = false;
            boolean hasWaveform = false;
            boolean extTriggered = false;
            boolean hasGate1 = false;
            boolean hasGate2 = false;
            boolean hasGate3 = false;
            boolean hasConstantGain = false;
            boolean hasp2p = false;
            boolean hasBandwidth = false;

            try {


        if (header == 0) {

            if (inputStream.available() > 32) {
                byte[] raw = inputStream.readNBytes(2);
                header = ((raw[0]&0xff)<<8) | ((raw[1]&0xff));

                while(((header >> 15) & 0x1) != 1){

                    header = ((header&0xff)<<8) | (inputStream.read()&0xff);

                    if(isDebug){
                        System.out.println("Wating for header! \n \t bin:"+Integer.toBinaryString(header));
                    }
                }

                //flagSet?
                if (((header >> 15) & 0x1) == 1) {

                    //header = ((data.get(0)&0xffff)<<16) | (data.get(1)&0xffff);
                    //System.out.println(Integer.toBinaryString(header));
                    stepNumber = (int) (header >> 10) & 0xf;

                    if(stepNumber == 0) {
                        update = true;
                    }

                    rxChannel = (int) (header >> 5) & 0xf;

                    txChannel = (int) (header) & 0xf;

                    //data.remove(0);

                    if(isDebug){
                        System.out.println("headerReader");
                        System.out.println("stepNumber: " + stepNumber);
                        System.out.println("rxChannel: " + rxChannel);
                        System.out.println("txChannel: " + txChannel);
                    }

                } else {
                    System.out.println("headerError");
                    header = 0;
                    //data.remove(0);
                }
            }
            //getMeta
            if (header > 0) {
                if (metaKey == 0) {
                    if (inputStream.available() > 4) {
                        byte[] raw = inputStream.readNBytes(2);
                        metaKey = ((raw[0]&0xff)<<8) | ((raw[1]&0xff));


                        if ((metaKey & 0x1) == 1) {
                            hasRange = true;
                        }
                        if (((metaKey >> 1) & 0x1) == 1) {
                            hasDelay = true;
                        }
                        if (((metaKey >> 2) & 0x1) == 1) {
                            hasWaveform = true;
                        }
                        if (((metaKey >> 3) & 0x1) == 1) {
                            extTriggered = true;
                        }
                        if (((metaKey >> 4) & 0x1) == 1) {
                            hasGate1 = true;
                        }
                        if (((metaKey >> 5) & 0x1) == 1) {
                            hasGate2 = true;
                        }
                        if (((metaKey >> 6) & 0x1) == 1) {
                            hasGate3 = true;
                        }
                        if (((metaKey >> 7) & 0x1) == 1) {
                            hasConstantGain = true;
                        }
                        if (((metaKey >> 8) & 0x1) == 1) {
                            hasp2p = true;
                        }
                        if (((metaKey >> 9) & 0x1) == 1) {
                            hasBandwidth = true;
                        }


                        if (isDebug) {
                            System.out.println("metaDataIncludes:");
                            if (hasRange) {

                                System.out.println("\tRange(Samples)");
                            }
                            if (hasDelay) {

                                System.out.println("\tDelay(Samples)");
                            }
                            if (hasWaveform) {

                                System.out.println("\twaveFormData");
                            }
                            if (extTriggered) {

                                System.out.println("\tEXT Triggered True");
                            }
                            if (hasGate1) {

                                System.out.println("\tGate 1 TOF");
                            }
                            if (hasGate2) {

                                System.out.println("\tGate 2 TOF");
                            }
                            if (hasGate3) {

                                System.out.println("\tGate 3 TOF");
                            }
                            if (hasConstantGain) {

                                System.out.println("\tConstantGain True");
                            }
                            if (hasp2p) {

                                System.out.println("\tPeak to Peak");
                            }
                            if (hasBandwidth) {

                                System.out.println("\tBandwidth");
                            }
                        }


                         }
                    else {
                        //System.out.println("metaError");
                        //header = 0;
                        //data.remove(0);
                    }

                }
            }

            if (metaKey > 0) {
                if (hasRange) {
                    byte[] raw = inputStream.readNBytes(4);
                    range =
                            ((((raw[0]&0xff)<<8 |(raw[1]&0xff)) & 0x3fff) << 15)
                            | (((raw[2]&0xff)<<8 |(raw[3]&0xff)) & 0x3fff);

                    if(isDebug) {
                        System.out.println("Range:"+range);
                    }
                }


                if (hasDelay) {
                    byte[] raw = inputStream.readNBytes(4);
                    delay =
                            ((((raw[0]&0xff)<<8 |(raw[1]&0xff)) & 0x3fff) << 15)
                                    | (((raw[2]&0xff)<<8 |(raw[3]&0xff)) & 0x3fff);

                    if(isDebug) {
                        System.out.println("Delay:"+delay);
                    }
                }
                if(hasWaveform) {
                    ArrayList<Integer> stepData = new ArrayList<Integer>();
                    for (int i = 0; i < range; i++) {
                        byte[] raw = inputStream.readNBytes(2);

                        stepData.add(
                                ((((raw[0] & 0xff) << 8) | (raw[1] & 0xff)) & 0x3fff));
                    }
                    data.set(stepNumber,stepData);


                }

                if (hasGate1) {
                    byte[] raw = inputStream.readNBytes(4);
                    gate1TOF =

                            ((((raw[0]&0xff)<<8 |(raw[1]&0xff)) & 0x3fff) << 15)
                                    | (((raw[2]&0xff)<<8 |(raw[3]&0xff)) & 0x3fff);

                    if(isDebug) {
                        System.out.println("Gate 1 TOF:"+(float)(gate1TOF/100000.0)+"us");
//                        System.out.print("\tbin: ");
//                        System.out.print(Integer.toBinaryString((raw[0]+256)%256));
//                        System.out.print(" ");
//                        System.out.println(Integer.toBinaryString((raw[1]+256)%256));
                    }
                }
                if (hasGate2) {
                    byte[] raw = inputStream.readNBytes(4);
                    gate2TOF =

                            ((((raw[0]&0xff)<<8 |(raw[1]&0xff)) & 0x3fff) << 15)
                                    | (((raw[2]&0xff)<<8 |(raw[3]&0xff)) & 0x3fff);

                    if(isDebug) {
                        System.out.println("Gate 2 TOF:"+(float)(gate2TOF/100000.0)+"us");
//                        System.out.print("\tbin: ");
//                        System.out.print(Integer.toBinaryString((raw[0]+256)%256));
//                        System.out.print(" ");
//                        System.out.println(Integer.toBinaryString((raw[1]+256)%256));
                    }
                }
                if (hasGate3) {
                    byte[] raw = inputStream.readNBytes(4);
                    gate3TOF =

                            ((((raw[0]&0xff)<<8 |(raw[1]&0xff)) & 0x3fff) << 15)
                                    | (((raw[2]&0xff)<<8 |(raw[3]&0xff)) & 0x3fff);

                    if(isDebug) {
                        System.out.println("Gate 3 TOF:"+(float)(gate3TOF/100000.0)+"us");
//                        System.out.print("\tbin: ");
//                        System.out.print(Integer.toBinaryString((raw[0]+256)%256));
//                        System.out.print(" ");
//                        System.out.println(Integer.toBinaryString((raw[1]+256)%256));
                    }
                }
                if(hasConstantGain){
                    constantCain = true;
                }
                if(hasp2p){
                    byte[] raw = inputStream.readNBytes(6);
                    peakToPeak = (((raw[0]&0xff)<<8 |(raw[1]&0xff)) & 0x3fff);
                    peakMin = (((raw[2]&0xff)<<8 |(raw[3]&0xff)) & 0x3fff);
                    peakMax = (((raw[4]&0xff)<<8 |(raw[5]&0xff)) & 0x3fff);

                    if(isDebug){
                        System.out.println("Peak To Peak: "+peakToPeak+"\n\tPeak Min: "+peakMin+"\n\tPeak Max: "+peakMax);

                    }

                }

                sqlInserter();

                //purge data - needs to be passing this to parent
                if(isDebug) {
                    System.out.println("Samples Available: "+data.size());
                }
                //System.out.println(data);
                data.removeAll(data);






            }

        }
                resetAll();


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }
}
