// Java implementation for a client
// Save file as Client.java

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.general.Dataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;

// Client class
public class xScanDataStreamHandlerORIGINAL {
    static JFreeChart aSCAN;
    static JFreeChart aSCANZoom;

    public static void main(String[] args) throws IOException {
        ArrayList<Integer> data = new ArrayList<Integer>();


        ArrayList<Integer> sampleData = new ArrayList<Integer>();
        sampleData.add(1);
        sampleData.add(2);

        aSCAN = ChartFactory.createXYLineChart(null, "Sample", "Amplitude", createDataset(sampleData), PlotOrientation.VERTICAL, false, true, false);
        aSCAN.getXYPlot().getRangeAxis().setRange(0, 16000);
        //aSCAN.getXYPlot().getDomainAxis().setRange(0,2000);
        ChartPanel panel = new ChartPanel(aSCAN);
        XYLineAndShapeRenderer renderer0 = new XYLineAndShapeRenderer(true,false);
        aSCAN.getXYPlot().setRenderer(0, renderer0);
        aSCAN.getXYPlot().getRendererForDataset(aSCAN.getXYPlot().getDataset(0)).setSeriesPaint(0, new Color(0x1E90FF));

        aSCANZoom = ChartFactory.createXYLineChart(null, "Sample", "Amplitude", createDataset(sampleData), PlotOrientation.VERTICAL, false, true, false);
        aSCANZoom.getXYPlot().getRangeAxis().setRange(0, 16000);
        //aSCAN.getXYPlot().getDomainAxis().setRange(0,2000);
        ChartPanel panelZoom = new ChartPanel(aSCANZoom);
        JSlider delaySlider = new JSlider(0, 0, 0);
        JSlider rangeSlider = new JSlider(0, 0);

        JFrame frame = new JFrame();
        frame.setSize(1200, 400);
        JPanel container = new JPanel();
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));

        container.add(panel);
        container.add(delaySlider);
        container.add(rangeSlider);
        container.add(panelZoom);
        frame.add(container);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        //Update Chart
        //quick frig to help out graphing multiple Steps

        boolean isPacket = false;
        long packetSize = 0;
        long packetCount = 0;

        int header = 0;
        int stepNumber = 0;
        int rxChannel = 0;
        int txChannel = 0;

        int metaKey = 0;
        int range = 0;
        int delay = 0;

        try {


            // getting localhost ip
            InetAddress ip = InetAddress.getByName("localhost");

            // establish the connection with server port 3000
            Socket s = new Socket("10.0.0.2", 3000);

            // obtaining input and out streams
            BufferedInputStream dis = new BufferedInputStream(s.getInputStream());


            // the following loop performs the exchange of
            // information between client and client handler


            int streamFrigger = 0;

            while (true) {
                //Quick Array Dump

                //Shitty chart Refresh
                if (range > 0) {

                    if (data.size() >= range) {
                        if (streamFrigger == 0) {


                            rangeSlider.setMaximum(range);
                            if (rangeSlider.getValue() == 0) {
                                rangeSlider.setValue(range);
                            }
                            if (delaySlider.getMaximum() == 0) {
                                delaySlider.setMaximum(range);
                            }
                            int d = delaySlider.getValue();
                            int r = rangeSlider.getValue();

                            aSCAN.getXYPlot().getDomainAxis().setRange(0, range);
                            aSCAN.getXYPlot().clearDomainMarkers();
                            ValueMarker de = new ValueMarker(d);
                            ValueMarker re = new ValueMarker(r);

                            aSCAN.getXYPlot().addDomainMarker(de);
                            aSCAN.getXYPlot().addDomainMarker(re);
                            aSCANZoom.getXYPlot().getDomainAxis().setRange(delaySlider.getValue(), rangeSlider.getValue());
                            XYDataset dataset = createDataset(data);
                            aSCAN.getXYPlot().setDataset(dataset);
                            aSCANZoom.getXYPlot().setDataset(dataset);


                            header = 0;
                            stepNumber = 0;
                            rxChannel = 0;
                            txChannel = 0;

                            metaKey = 0;
                            range = 0;
                            delay = 0;


                            for (int i = 0; i < range; i++) {

                                data.remove(0);
                            }
                            streamFrigger = 0;
                        } else {
                            streamFrigger += 1;
                            header = 0;
                            stepNumber = 0;
                            rxChannel = 0;
                            txChannel = 0;

                            metaKey = 0;
                            range = 0;
                            delay = 0;
                            for (int i = 0; i < range; i++) {

                                data.remove(0);
                            }
                        }
                    }
                }


                //read inputStream
                if (dis.available() > 16) {
                    //System.out.println("bufferSize:" + dis.available());
                    if (isPacket == false) {
                        int[] raw = new int[4];
                        raw[0] = dis.read();
                        raw[1] = dis.read();
                        raw[2] = dis.read();
                        raw[3] = dis.read();
                        packetSize = 0;
                        packetSize = ((raw[0] & 0xff) << 24) | ((raw[1] & 0xff) << 16) | ((raw[2] & 0xff) << 8) | (raw[3] & 0xff);

                        packetSize = packetSize / 2;

                        //System.out.println("PacketSize:" + packetSize);
                        isPacket = true;


                    }
                    if (isPacket == true) {
                        int[] raw = new int[2];

                        raw[0] = dis.read();
                        raw[1] = dis.read();

                        int sample = ((raw[0] << 8) & 0xff00) | (raw[1] & 0xff);
                        //System.out.println(sample);
                        packetCount += 1;
                        data.add(sample);
//                        ////System.out.println("packetSize:"+packetSize+"\npacketCount:"+packetCount);
                        if (packetCount == packetSize) {
                            //  System.out.println("end of package");
                            isPacket = false;
                            packetCount = 0;
                        }

                    }
                    //
                }

                if (header == 0) {

                    if (data.size() > 16) {
                        header = data.get(0);
                        header = header & 0xffff;
                        if (((header >> 15) & 0x1) == 1) {
                            //System.out.println("headerReader");
                            //header = ((data.get(0)&0xffff)<<16) | (data.get(1)&0xffff);
                            //System.out.println(Integer.toBinaryString(header));
                            stepNumber = (int) (header >> 10) & 0xf;
                            //System.out.println("stepNumber: " + stepNumber);
                            rxChannel = (int) (header >> 5) & 0xf;
                            //System.out.println("rxChannel: " + rxChannel);
                            txChannel = (int) (header) & 0xf;
                            //System.out.println("txChannel: " + txChannel);
                            data.remove(0);

                        } else {
                            //System.out.println("headerError");
                            header = 0;
                            data.remove(0);
                        }
                    }
                    //getMeta
                    if (header > 0) {
                        if (metaKey == 0) {
                            if (data.size() > 4) {
                                metaKey = data.get(0);
                                metaKey = metaKey & 0xffff;
                                //System.out.println("metaDataIncludes:");
                                if ((metaKey & 0x1) == 1) {

                                    //System.out.println("\tRange(Samples)");
                                }
                                if (((metaKey >> 1) & 0x1) == 1) {

                                    //System.out.println("\tDelay(Samples)");
                                }
                                if (((metaKey >> 2) & 0x1) == 1) {

                                    //System.out.println("\twaveFormData");
                                }
                                if (((metaKey >> 3) & 0x1) == 1) {

                                    //System.out.println("\ttriggerState");
                                }
                                data.remove(0);


                            } else {
                                //System.out.println("metaError");
                                //header = 0;
                                //data.remove(0);
                            }

                        }
                    }

                    if (metaKey > 0) {
                        if (range == 0) {
                            range = ((data.get(0) & 0x3fff) << 15) | (data.get(1) & 0x3fff);
                            data.remove(0);
                            data.remove(0);
                            //System.out.println("Range:"+range);
                        }


                        if (delay == 0) {
                            delay = ((data.get(0) & 0x3fff) << 15) | (data.get(1) & 0x3fff);
                            data.remove(0);
                            data.remove(0);
                            //System.out.println("Delay: "+delay);


                        }
                    }

                }

            }


            // closing resources


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static XYDataset createDataset(ArrayList<Integer> data) {
        final XYSeries samples = new XYSeries("Step 0");

        for (int i = 0; i < data.size(); i++) {

            samples.add(i, data.get(i));
        }


        final XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(samples);
        return dataset;
    }
}