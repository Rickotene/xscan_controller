import java.lang.reflect.Field;
import java.util.LinkedHashMap;

public class jsonHandler {
    String term = "*\n";

    String jsonCreator(String command, String data) {
        org.json.JSONObject json = new org.json.JSONObject();

        try {
            Field changeMap = json.getClass().getDeclaredField("map");
            changeMap.setAccessible(true);
            changeMap.set(json, new LinkedHashMap<>());
            changeMap.setAccessible(false);
        } catch (IllegalAccessException | NoSuchFieldException e) {

        }
        json.put("CMD", command);
        json.put("Data", data);
        return json.toString() + term;
    }

    String jsonCreator(String command, int data) {
        org.json.JSONObject json = new org.json.JSONObject();

        try {
            Field changeMap = json.getClass().getDeclaredField("map");
            changeMap.setAccessible(true);
            changeMap.set(json, new LinkedHashMap<>());
            changeMap.setAccessible(false);
        } catch (IllegalAccessException | NoSuchFieldException e) {

        }
        json.put("CMD", command);
        json.put("Data", data);
        return json.toString() + term;
    }

    String jsonCreator(String command, String[] data, Boolean[] values) {
        org.json.JSONObject json = new org.json.JSONObject();
        try {
            Field changeMap = json.getClass().getDeclaredField("map");
            changeMap.setAccessible(true);
            changeMap.set(json, new LinkedHashMap<>());
            changeMap.setAccessible(false);
        } catch (IllegalAccessException | NoSuchFieldException e) {

        }
        json.put("CMD", command);
        org.json.JSONObject dataArray = new org.json.JSONObject();
        try {
            Field changeMap2 = dataArray.getClass().getDeclaredField("map");
            changeMap2.setAccessible(true);
            changeMap2.set(dataArray, new LinkedHashMap<>());
            changeMap2.setAccessible(false);
        } catch (IllegalAccessException | NoSuchFieldException e) {

        }
        for (int i = 0; i < data.length; i++) {
            dataArray.put(data[i], values[i]);
        }
        json.put("Data", dataArray);
        return json.toString() + term;
    }

    ;

    String jsonCreator(String command, String[] data, int[] values) {
        org.json.JSONObject json = new org.json.JSONObject();
        try {
            Field changeMap = json.getClass().getDeclaredField("map");
            changeMap.setAccessible(true);
            changeMap.set(json, new LinkedHashMap<>());
            changeMap.setAccessible(false);
        } catch (IllegalAccessException | NoSuchFieldException e) {

        }
        json.put("CMD", command);
        org.json.JSONObject dataArray = new org.json.JSONObject();
        try {
            Field changeMap2 = dataArray.getClass().getDeclaredField("map");
            changeMap2.setAccessible(true);
            changeMap2.set(dataArray, new LinkedHashMap<>());
            changeMap2.setAccessible(false);
        } catch (IllegalAccessException | NoSuchFieldException e) {

        }
        for (int i = 0; i < data.length; i++) {
            dataArray.put(data[i], values[i]);
        }
        json.put("Data", dataArray);
        return json.toString() + term;
    }

    ;

    String jsonCreator(String command, int[] data) {
        org.json.JSONObject json = new org.json.JSONObject();

        try {
            Field changeMap = json.getClass().getDeclaredField("map");
            changeMap.setAccessible(true);
            changeMap.set(json, new LinkedHashMap<>());
            changeMap.setAccessible(false);
        } catch (IllegalAccessException | NoSuchFieldException e) {

        }
        json.put("CMD", command);
        json.put("Data", data);
        return json.toString() + term;
    }

    String jsonCreator(String command, double[] data) {
        org.json.JSONObject json = new org.json.JSONObject();

        try {
            Field changeMap = json.getClass().getDeclaredField("map");
            changeMap.setAccessible(true);
            changeMap.set(json, new LinkedHashMap<>());
            changeMap.setAccessible(false);
        } catch (IllegalAccessException | NoSuchFieldException e) {

        }
        json.put("CMD", command);
        json.put("Data", data);
        return json.toString() + term;
    }

    String jsonCreator(String command, boolean[] data) {
        org.json.JSONObject json = new org.json.JSONObject();

        try {
            Field changeMap = json.getClass().getDeclaredField("map");
            changeMap.setAccessible(true);
            changeMap.set(json, new LinkedHashMap<>());
            changeMap.setAccessible(false);
        } catch (IllegalAccessException | NoSuchFieldException e) {

        }
        json.put("CMD", command);
        json.put("Data", data);
        return json.toString() + term;
    }


}


