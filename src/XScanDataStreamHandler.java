
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;

// Client class
public class XScanDataStreamHandler extends Thread {

    boolean isDebug = false;
    boolean isPacket = false;
    long packetSize = 0;
    long packetCount = 0;


    //trying to seperate the parser

    //stream from xSCAN data stream
    PipedOutputStream outputStream;

    public XScanDataStreamHandler(PipedOutputStream output) {
        this.outputStream = output;
    }

    public void aquire(String ipAddress, int port, boolean debug) throws IOException {


        try {


            // getting localhost ip
            InetAddress ip = InetAddress.getByName(ipAddress);

            // establish the connection with server port 5056
            Socket s = new Socket(ip, port);
//            s.setSoTimeout(10*1000);

            // obtaining input and out streams
            DataInputStream dis = new DataInputStream(s.getInputStream());


            // the following loop performs the exchange of
            // information between client and client handler


            while (true) {

                //read inputStream

                //System.out.println("bufferSize:" + dis.available());
                if (isPacket == false) {
                    byte[] raw = new byte[4];
                    raw[0] = dis.readByte();
                    raw[1] = dis.readByte();
                    raw[2] = dis.readByte();
                    raw[3] = dis.readByte();
                    packetSize = 0;
                    packetSize = ((raw[0] & 0xff) << 24) | ((raw[1] & 0xff) << 16) | ((raw[2] & 0xff) << 8) | (raw[3] & 0xff);

                    packetSize = packetSize / 2;

                    //System.out.println("PacketSize:" + packetSize);
                    isPacket = true;


                }
                if (isPacket == true) {
                    byte[] raw = new byte[2];

                    raw[0] = dis.readByte();
                    raw[1] = dis.readByte();

                    //int sample = ((raw[0] << 8) & 0xff00) | (raw[1] & 0xff);
                    //System.out.println(sample);
                    packetCount += 1;
                    outputStream.write(raw);
//                        ////System.out.println("packetSize:"+packetSize+"\npacketCount:"+packetCount);
                    if (packetCount == packetSize) {
                        //  System.out.println("end of packet");
                        isPacket = false;
                        packetCount = 0;
                    }

                }
                //


            }


            // closing resources


        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Stream Error");
        }
    }


    @Override
    public void run() {

        try {
            aquire("10.0.0.2", 3001, true);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}

