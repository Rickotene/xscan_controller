import javax.swing.*;
import java.awt.*;

public class DepthButton extends JButton {
    int max = 16777216/16;
    int[] colours = {0x1E90FF,0xFF0000,0x00FF00,0x0000FF,0xFFFF00,0x00FFFF,0xFF00FF,0xC0C0C0,0x808080,0x800000,0x808000,0x008000,0x800080,0x008080,0x000080,0xADFF2F,0xffffff
    };
    /** Creates a new instance of DepthButton */
    int channel;
    public DepthButton(String text,int channelNo) {
        super(text);
        setContentAreaFilled(false);
        channel = channelNo;
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        String text = this.getText();
        String[]spl = text.split(" ");
        //int channel = Integer.parseInt(spl[1]);

        GradientPaint p;
        p = new GradientPaint(0, 0, getBackground(), 0, getHeight(), new Color(colours[channel]));

        Paint oldPaint = g2.getPaint();
        g2.setPaint(p);
        g2.fillRect(0, 0, getWidth(), getHeight());
        g2.setPaint(oldPaint);

        super.paintComponent(g);
    }
}