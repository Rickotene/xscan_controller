import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedReader;
import java.io.Reader;
import java.io.StringReader;
import java.nio.channels.CompletionHandler;
import java.nio.charset.Charset;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class XScanConfigHandler implements CompletionHandler<Integer, Attachment> {

    String xScanResponse = "";

    String userCommand = "";
    PropertyChangeSupport support = new PropertyChangeSupport(xScanResponse);



    public void addPropertyChangeListener(PropertyChangeListener listener) {
        support.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        support.removePropertyChangeListener(listener);
    }

    public String getMessage() {
        return this.userCommand;
    }

    public void setMessage(String newValue) {
        String oldValue = this.userCommand;
        this.userCommand = newValue;
        resetKeepAlive();

    }

    //KeepAlive*******************************************************************//
    class KeepAlive extends TimerTask {
        @Override
        public void run() {
            jsonHandler msg = new jsonHandler();
            String json = msg.jsonCreator("Get Time ISO8601", 0) + "*\n";
            setMessage(json);
        }
    }

    Timer keepAliveTimer = new Timer();
    KeepAlive keepAlive = new KeepAlive();

    public void startKeepAlive(){
        keepAliveTimer = new Timer();
        keepAlive = new KeepAlive();
        keepAliveTimer.schedule(keepAlive,15000,15000);
    }

    public void resetKeepAlive(){
        keepAliveTimer.cancel();
        keepAliveTimer = new Timer();
        keepAlive = new KeepAlive();
        keepAliveTimer.schedule(keepAlive,15000,15000);
    }

    @Override
    public void completed(Integer result, Attachment attach) {
        if (attach.isRead) {
            attach.buffer.flip();
            Charset cs = Charset.forName("UTF-8");
            int limits = attach.buffer.limit();

            byte bytes[] = new byte[limits];

            attach.buffer.get(bytes, 0, limits);
            //System.out.println("Current Buffer size ="+bytes.length);
            String msg = new String(bytes, cs);
            msg = msg.substring(0,msg.length()-1);

            //System.out.println(msg);
            String oldValue = xScanResponse;
            xScanResponse = msg;
            support.firePropertyChange("message", oldValue, xScanResponse);





            try {
                msg = this.getTextFromUser();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (msg.equalsIgnoreCase("bye")) {
                attach.mainThread.interrupt();
                return;
            }

            attach.buffer.clear();
            byte[] data = msg.getBytes(cs);
            attach.buffer.put(data);
            attach.buffer.flip();
            attach.isRead = false; // It is a write
            attach.channel.write(attach.buffer, attach, this);

        } else {
            attach.isRead = true;
            attach.buffer.clear();
            attach.channel.read(attach.buffer, attach, this);
        }
    }

    @Override
    public void failed(Throwable e, Attachment attach) {
        e.printStackTrace();
    }

    public String getTextFromUser() throws Exception {

        String msg = null;

        while (msg == null) {

            Reader inputString = new StringReader(userCommand);
            BufferedReader reader = new BufferedReader(inputString);
            if (!userCommand.equals("")) {
                msg = reader.readLine();
                //System.out.println("\n"+msg);
            }
        }


        //System.out.println("Read");
        if (msg.equals("bye")) {
        } else if (msg.contains("{")) {
        } else {
            String[] split = msg.split(":");
            msg = "{\"CMD\":\"" + split[0] + "\",\"Data\":" + split[1] + "}*";

        }
        userCommand = "";
        return msg;
    }
}

