import java.text.SimpleDateFormat;
import java.util.Date;
import java.sql.*;

public class SQLCommandsUpdate {



    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://192.168.2.52:3306/XScanDataLogger";

    //  Database credentials
    static final String USER = "R.Shepherd";
    static final String PASS = "";



    public void saveTOF(int[] ints){
        //stepNumber,rxChannel,txChannel,range,delay,triggerState,gate1TOF,gate2TOF,gate3TOF,constantCain,peakToPeak,peakMin,peakMax

        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        Connection conn = null;
        Statement stmt = null;
        int rs = 0;
        try{
            //STEP 2: Register JDBC driver
           Class.forName(JDBC_DRIVER);

            //STEP 3: Open a connection
            //System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

            //STEP 4: Execute a query
            //System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;


            int[]data = ints;
//            String[] data = new String[ints.length];
//            for(int i =0; i < ints.length;i++){
//                data[i] = Integer.toString(ints[i]);
//            }
//            sql = "INSERT INTO TimeOfFlight (SerialNo, stepNumber, rxChannel, txChannel, range, delay, gate1TOF, gate2TOF, gate3TOF, peakToPeak, peakMin, peakMax, TimeStamp) VALUES ('01e35f9', '"+data[0]+"', '"+data[1]+"', '"+data[2]+"', '"+data[3]+"', '"+data[4]+"', '"+data[5]+"', '"+data[6]+"', '"+data[7]+"', '"+data[8]+"', '"+data[9]+"', '"+data[10]+"', '"+timeStamp+"')";

            sql = "INSERT INTO TimeOfFlight (SerialNo, stepNumber, rxChannel, txChannel, gate1TOF, gate2TOF, gate3TOF, peakToPeak, peakMin, peakMax) VALUES ('01e35f9', "+data[0]+", "+data[1]+", "+data[2]+", "+data[5]+", "+data[6]+", "+data[7]+", "+data[8]+", "+data[9]+", "+data[10]+")";
            //SerialNumber, RaisedBy, FaultDescription, Comments, CurrentOwner, IsClosed, ClosedBy
            //System.out.println(sql);

//demo setup
//            testBench = new SQLCommandsUpdate();
//            testBench.saveTOF(new int[]{stepNumber, rxChannel, txChannel, range, delay, gate1TOF, gate2TOF, gate3TOF, peakToPeak, peakMin, peakMax});


            stmt.executeUpdate(sql);
            //stmt.executeUpdate("INSERT INTO NonConformance (SerialNumber, RaisedBy, FaultDescription) VALUES ('55501362', 'RDS', 'Its Bust Again')");


            //STEP 5: Extract data from result set
            //while(rs.next()){
            //Retrieve by column name
            //OperatorTile New = new OperatorTile(rs.getInt("OperatorID"), Operators.length );
            //  Name = rs.getString("OperatorName");
            //Operators = (OperatorTile[])append(Operators, New);

            //}
            //STEP 6: Clean-up environment
            //rs.close();
            stmt.close();
            conn.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try
        //System.out.println("Goodbye!");

        //return Name;
    }

    public void sqlInsert(int[] data){


        saveTOF(data);

    }

}

