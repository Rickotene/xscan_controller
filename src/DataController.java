
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.charset.Charset;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.beans.*;

//from   w w  w  . j av a 2 s  .  c om
public class DataController {


    public static void main(String[] args) throws Exception {


        //***********************************************************************//
        //Start Gui
        XScanGuiFull gui = new XScanGuiFull();


        //***********************************************************************//
        //StartConfig Handler
        XScanConfigHandler xScanConfigHandler = new XScanConfigHandler();

        //***********************************************************************//
        //start dataStream



        //***********************************************************************//
        //Start Keep Alive timer
        xScanConfigHandler.startKeepAlive();

        //***********************************************************************//
        //Listen for Config Message from xSCAN and send to gui
        PropertyChangeListener xScanConfigListener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                gui.setConfig ((String)evt.getNewValue());
            }
        };
        xScanConfigHandler.support.addPropertyChangeListener(xScanConfigListener);
        //***********************************************************************//
        //Listen for Config Message from Gui and send to xSCAN
        PropertyChangeListener guiConfigListener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                xScanConfigHandler.setMessage ((String)evt.getNewValue());

            }
        };
        gui.cmd.addPropertyChangeListener(guiConfigListener);

        //Listen for update from gui Parser




        AsynchronousSocketChannel channel = AsynchronousSocketChannel.open();
        SocketAddress serverAddr = new InetSocketAddress("10.0.0.2", 2000);
        Future<Void> result = channel.connect(serverAddr);

        result.get(60, TimeUnit.SECONDS);
        System.out.println("Connected");
        Attachment attach = new Attachment();
        attach.channel = channel;
        attach.buffer = ByteBuffer.allocate(4096*4);
        attach.isRead = false;
        attach.mainThread = Thread.currentThread();

        Charset cs = Charset.forName("UTF-8");
        String msg = "{\"CMD\":\"Get Steps\",\"Data\":0}*";
        byte[] data = msg.getBytes(cs);
        attach.buffer.put(data);
        attach.buffer.flip();
        channel.write(attach.buffer, attach, xScanConfigHandler);
        attach.mainThread.join();




    }
}

